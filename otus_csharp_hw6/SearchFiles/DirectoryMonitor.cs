﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw6.SearchFiles
{
    public class DirectoryMonitor
    {
   
        public void Read(string path)
        {
            var directoryInfo = new DirectoryInfo(path);
            foreach (FileInfo fileInfo in directoryInfo.EnumerateFiles())
            {
                var e = new FileArgs() { Name = fileInfo.Name };
                FileFound?.Invoke(this, e);

                if (e.Cancel)
                {
                    break;
                }
            }
        }

        public delegate void EventHandler(object sender, FileArgs e);
        public event EventHandler FileFound;
    }

    public class FileArgs : EventArgs
    {
        public bool Cancel;
        public string Name;
    }
}
