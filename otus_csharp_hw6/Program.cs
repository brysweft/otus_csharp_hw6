﻿using otus_csharp_hw6.SearchExtension;
using otus_csharp_hw6.SearchFiles;

// *** Поиск максимального элемента в коллекции

string path = @"C:\Users\obs19\OneDrive\Документы\Test";
bool Cancel = false;
List<string> foundFiles = new List<string>();

var directoryMonitor = new DirectoryMonitor();

Console.WriteLine("Обход каталога файлов {0}", path);

directoryMonitor.FileFound += FileFoundEventHandler;
Console.CancelKeyPress += ConsoleCancelEventHandler;

directoryMonitor.Read(path);

FoundMaxFile();

Dispose();

void FoundMaxFile()
{
    string maxItem = foundFiles.GetMax<string>(GetParameterToFloat);
    Console.WriteLine("Максимальный элемент {0}", maxItem);
}

void FileFoundEventHandler(object sender, FileArgs e) {
    Console.WriteLine("Найден файл {0}", e.Name);
    foundFiles.Add(e.Name);
    // Добавил паузу для теста отмены в консоли (Ctrl + Break)
    System.Threading.Thread.Sleep(1000);
    e.Cancel = Cancel;
}

float GetParameterToFloat(string fileName)
{
    return fileName.Length;
}

void ConsoleCancelEventHandler(object? sender, ConsoleCancelEventArgs? e)
{
    Cancel = true;
    Console.WriteLine("Поиск отменен");

    FoundMaxFile();
    Dispose();
}

void Dispose()
{
    directoryMonitor.FileFound -= FileFoundEventHandler;
    Console.CancelKeyPress -= ConsoleCancelEventHandler;
}