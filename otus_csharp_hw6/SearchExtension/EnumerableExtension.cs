﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_csharp_hw6.SearchExtension
{

    public delegate float ParameterToFloat<T>(T value);

    public static class EnumerableExtension
    {
        public static T GetMax<T>(this IEnumerable<T> e, ParameterToFloat<T> getParameter) where T : class
        {
            T max = null;
            float value = 0;
            float maxValue = 0;

            foreach (var item in e) {
                value = getParameter(item);
                if (value >= maxValue)
                {
                    max = item;
                    maxValue = value;  
                }
            }

            return max;
        }
    }
}
